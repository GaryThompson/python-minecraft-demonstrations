# Copyright: (c) 2022, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
"""
A websocket server trial

This code is quite messy as it is part of a proof of concept.

Once the protocol details are better understood a cleaner implementation
will be designed.
"""
import enum
import json
import logging
import asyncio
from typing import Optional, Union
from uuid import uuid4

import websockets

logger = logging.getLogger(__name__)

LISTEN_ADDRESS = "localhost"
LISTEN_PORT = 8765
connection_count = 0

run_on_connect: callable = lambda: None

chat_callbacks = {}
compatibility_global_socket = None


class EventTypes:
    PlayerMessage = "PlayerMessage"


def is_message_request(msg: dict) -> bool:
    header = msg.get("header")
    if header:
        purpose = header.get("messagePurpose")
        return purpose == "commandRequest"
    else:
        return False


def is_message_response(msg: dict) -> bool:
    header = msg.get("header")
    if header:
        purpose = header.get("messagePurpose")
        return purpose == "commandResponse"
    else:
        return False


def get_message_request_id(msg: dict) -> Optional[str]:
    if is_message_request(msg) or is_message_response(msg):
        return msg["header"]["requestId"]
    else:
        return None


def print_message(traffic, msg: Union[dict, str, bytes]):
    if isinstance(msg, bytes):
        print(f"Encryption is not supported\n{msg}")
    else:
        if isinstance(msg, str):
            msg_data = json.loads(msg)
        else:
            msg_data = msg
        pretty_msg = json.dumps(msg_data, indent=4)
        log_text = f"\n{traffic}  {pretty_msg}"
        print(log_text)
        # with open("docs/communication.log", "a") as f:
        #     f.write(log_text)


def minecraft_protocol_v1_subscribe_message(
    event_name: str,
) -> dict:
    """
    Constructs a message to send to the minecraft application
    """
    return {
        "header": {
            "version": 1,  # We're using the version 1 message protocol
            "requestId": str(uuid4()),  # A unique ID for the request
            "messageType": "commandRequest",
            "messagePurpose": "subscribe",
        },
        "body": {"eventName": event_name},
    }


def minecraft_protocol_v1_command_line_message(
    command_line: str,
) -> dict:
    """
    Sends a command to the server
    """
    return {
        "header": {
            "requestId": str(uuid4()),  # A unique ID for the request
            "version": 1,  # We're using the version 1 message protocol
            "messagePurpose": "commandRequest",
            "messageType": "commandRequest",
        },
        "body": {
            "origin": {"type": "player"},
            "commandLine": command_line,
            "version": 1,
        },
    }


def is_event_message(msg: dict) -> bool:
    header = msg.get("header")
    if header:
        purpose = header.get("messagePurpose")
        return purpose == "event"
    else:
        return False


def get_event_type(msg: dict) -> Optional[str]:
    if is_event_message(msg):
        return msg["body"]["eventName"]
    else:
        return None


async def enable_player_messages(
    minecraft_socket: websockets.WebSocketServerProtocol,
):
    """
    Creates a chat listener - this will intercept chat messages
    """
    cmd = minecraft_protocol_v1_subscribe_message(EventTypes.PlayerMessage)
    await minecraft_socket.send(json.dumps(cmd))


async def listen_to_player_messages(
    minecraft_socket: websockets.WebSocketServerProtocol,
):
    """
    Will execute callback commands based on the player message
    """
    global chat_callbacks
    async for message in minecraft_socket:
        msg_data = json.loads(message)
        if (
            get_event_type(msg_data) == EventTypes.PlayerMessage
            and msg_data["body"]["properties"]["MessageType"] == "chat"
        ):
            chat_message = msg_data["body"]["properties"]["Message"]
            sender = msg_data["body"]["properties"]["Sender"]
            if chat_message in chat_callbacks:
                chat_callbacks[chat_message]()
            else:
                logger.debug("Chat Message from %s: %s", sender, chat_message)
        else:
            print_message("Minecraft --> this", message)
            # logger.debug("Message received: %s", message)


async def exec_command_line_command(
    minecraft_socket: websockets.WebSocketServerProtocol, cmd: str
) -> dict:
    """
    This is to simplify executing a message on the command line and getting a response.
    This will ensure the appropriate message is sent and received.
    :returns:  The body of the response message
    """
    cmd = minecraft_protocol_v1_command_line_message(cmd)
    req_id = get_message_request_id(cmd)
    await minecraft_socket.send(json.dumps(cmd))
    result = json.loads(await minecraft_socket.recv())

    assert is_message_response(result)
    assert get_message_request_id(result) == req_id
    return result["body"]


async def get_edu_client_info(minecraft_socket: websockets.WebSocketServerProtocol):
    """
    API Reference:  https://minecraft.fandom.com/wiki/Commands/geteduclientinfo
    """
    # noinspection SpellCheckingInspection
    await exec_command_line_command(minecraft_socket, "geteduclientinfo")


async def get_local_player_name(
    minecraft_socket: websockets.WebSocketServerProtocol,
) -> str:
    """
    API Reference:  https://minecraft.fandom.com/wiki/Commands
    """
    # noinspection SpellCheckingInspection
    ret = await exec_command_line_command(minecraft_socket, "getlocalplayername")
    # noinspection SpellCheckingInspection
    return ret["localplayername"]


async def query_target(
    minecraft_socket: websockets.WebSocketServerProtocol, target_name: str
) -> dict:
    """
    API Reference:  https://minecraft.fandom.com/wiki/Commands/querytarget
    """
    # noinspection SpellCheckingInspection
    ret = await exec_command_line_command(
        minecraft_socket, f"querytarget {target_name}"
    )
    data = json.loads(ret["details"])
    # print_message("Command Line Minecraft --> this", result)
    return data


class Axis(enum.Enum):
    X = "X"
    Y = "Y"
    Z = "Z"


class Block(enum.Enum):
    """
    The following links help work out some of these titles:

    https://minecraft.fandom.com/wiki/Bedrock_Edition_data_values
    """

    AIR = "air"
    # COARSE_DIRT = "coarse_dirt"  # This has state value in Bedrock Edition to make it course
    DIRT = "dirt"
    GRASS = "grass"
    GRAVEL = "gravel"
    LAVA = "lava"
    MAGMA_BLOCK = "magma"
    STONE = "stone"


class CloneMask(enum.Enum):
    """
    API REF: https://minecraft.fandom.com/wiki/Commands/clone
    :cvar REPLACE:  Copy all blocks, overwriting all blocks of the destination
                    region with the blocks from the source region.
    :cvar MASKED:  Copy only non-air blocks. Blocks in the destination region
                   that would otherwise be overwritten by air are left unmodified
    """

    REPLACE = "replace"
    MASKED = "masked"


class CloneMode(enum.Enum):
    """
    API REF:  https://minecraft.fandom.com/wiki/Commands/clone
    :cvar FORCE:  Force the clone even if the source and destination regions overlap.
    :cvar MOVE:  Clone the source region to the destination region, then replace
                 the source region with air. When used in filtered mask mode,
                 only the cloned blocks are replaced with air.
    :cvar NORMAL:  Don't move or force.
    """

    FORCE = "force"
    MOVE = "move"
    NORMAL = "normal"


class FillMode(enum.Enum):
    """
    API REF:  https://minecraft.fandom.com/wiki/Commands/fill#Result
    """

    DESTROY = "destroy"
    HOLLOW = "hollow"
    KEEP = "keep"
    OUTLINE = "outline"
    REPLACE = "replace"


class Position:
    """
    X:  Runs East and West with East being the positive/increasing direction
    Y:  Runs up and down with up being the positive/increasing direction
    Z:  Runs North and South with South being the positive/increasing direction
    """

    def __init__(self, x: int, y: int, z: int, is_relative=True):
        self.x = int(x)
        self.y = int(y)
        self.z = int(z)
        self.is_relative = is_relative

    def get_value(self, axis: Axis) -> int:
        """
        Compatibility function
        """
        if axis == Axis.X:
            return int(self.x)
        elif axis == Axis.Y:
            return int(self.y)
        elif axis == Axis.Z:
            return int(self.z)
        else:
            raise ValueError(f"Unsupported axis value:  {axis}")

    def equals(self, other):
        if self.is_relative != other.is_relative:
            return False
        else:
            return self.x == other.x and self.y == other.y and self.z == other.z

    def offset(self, x_amount=0, y_amount=0, z_amount=0):
        return Position(
            x=self.x + x_amount,
            y=self.y + y_amount,
            z=self.z + z_amount,
            is_relative=self.is_relative,
        )

    def set_x(self, val):
        return Position(
            x=val,
            y=self.y,
            z=self.z,
            is_relative=self.is_relative,
        )

    def set_y(self, val):
        return Position(
            x=self.x,
            y=val,
            z=self.z,
            is_relative=self.is_relative,
        )

    def set_z(self, val):
        return Position(
            x=self.x,
            y=self.y,
            z=val,
            is_relative=self.is_relative,
        )

    def __str__(self):
        if self.is_relative:
            return f"~{self.x:.0f} ~{self.y:.0f} ~{self.z:.0f}"
        else:
            return f"{self.x:.0f} {self.y:.0f} {self.z:.0f}"

    def __add__(self, other: "Position") -> "Position":
        return Position(
            self.x + other.x,
            self.y + other.y,
            self.z + other.z,
            is_relative=self.is_relative and other.is_relative,
        )

    def __sub__(self, other: "Position") -> "Position":
        return Position(
            self.x - other.x,
            self.y - other.y,
            self.z - other.z,
            is_relative=self.is_relative and other.is_relative,
        )


async def fill(
    minecraft_socket: websockets.WebSocketServerProtocol,
    from_position: Position,
    to_position: Position,
    block_type: Block,
) -> int:
    """
    API Reference:  https://minecraft.fandom.com/wiki/Commands/fill
    """
    # noinspection SpellCheckingInspection
    ret = await exec_command_line_command(
        minecraft_socket, f"fill {from_position} {to_position} {block_type.value}"
    )
    if "fillCount" not in ret:
        print_message("Command Line Minecraft --> this", ret)
        return 0
    else:
        fill_count = ret["fillCount"]
        block_name = ret["blockName"]
        logger.info("Fill %i %s blocks", fill_count, block_name)
        return fill_count


async def clone(
    minecraft_socket: websockets.WebSocketServerProtocol,
    select_start: Position,
    select_end: Position,
    destination: Position,
    mask: CloneMask,
    mode: CloneMode,
) -> int:
    """
    API Reference:  https://minecraft.fandom.com/wiki/Commands/clone
    """
    # noinspection SpellCheckingInspection
    ret = await exec_command_line_command(
        minecraft_socket,
        f"clone {select_start} {select_end} {destination} {mask.value} {mode.value}",
    )
    if "count" not in ret:
        print_message("Command Line Minecraft --> this", ret)
        return 0
    else:
        clone_count = ret["count"]
        logger.info("Cloned %i blocks", clone_count)
        return clone_count


async def minecraft_connection(websocket: websockets.WebSocketServerProtocol):
    global compatibility_global_socket, run_on_connect
    logger.debug("New MineCraft connection: %s", websocket)
    compatibility_global_socket = websocket

    await get_edu_client_info(websocket)
    await enable_player_messages(websocket)

    if run_on_connect:
        run_on_connect()

    # There needs to be a message queue that all commands can
    # listen to instead of this synchronous process.  The moment
    # someone chats multiple times, this will get very stuck.
    await listen_to_player_messages(websocket)

    async for message in websocket:
        print_message("Minecraft --> this", message)
        logger.debug("Message received: %s", message)


def start_server():
    """
    For the moment, the design is to have the websocket server as the
    main thread and then issue commands in sub-threads.
    """
    logging.getLogger("websockets.server").setLevel(logging.INFO)
    logging.basicConfig(level=logging.DEBUG)

    async def main():
        async with websockets.serve(minecraft_connection, LISTEN_ADDRESS, LISTEN_PORT):
            print(
                "Don't forget to disable encrypted websockets in Settings -> General -> Profile\n"
                "This is available outside of a running game but is locked if a game has started."
            )
            print(f"In Minecraft, type in /connect {LISTEN_ADDRESS}:{LISTEN_PORT}")
            await asyncio.Future()  # run forever

    asyncio.run(main(), debug=True)


if __name__ == "__main__":
    start_server()
