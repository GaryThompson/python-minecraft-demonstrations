# Copyright: (c) 2022, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
"""
Implement the Plates Trial via the websocket
"""
import logging
import nest_asyncio

# required by compatability
nest_asyncio.apply()


def main():
    """
    This works by triggering the plates_trial script to
    execute and access global variables.  It is a
    dirty hack for now.
    """

    def on_connection():
        import plates_trial

        plates_trial.player.enable_compatibility()

    import ws_server_trial

    ws_server_trial.run_on_connect = on_connection
    ws_server_trial.start_server()


if __name__ == "__main__":
    logging.getLogger("websockets.server").setLevel(logging.INFO)
    logging.basicConfig(level=logging.DEBUG)

    main()
