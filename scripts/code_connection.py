# Copyright: (c) 2022, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
"""
This is experimental code.

This module provides an interface to Minecraft Code Connection.  Please ensure
it is connected to minecraft before beginning.

Setup Notes:

https://educommunity.minecraft.net/hc/en-us/articles/360047555251

Note that Code Connection is a superseded by the built-in code editor, although it does not appear to be
lost just yet.

Just in case, this API will aim to mimic the built-in code editor in case
code connection becomes defunct.  This should simplify porting of any
code.
"""
import json

import requests

BASE_URL = "http://localhost:8080"


class Directions:
    forward = "forward"
    back = "back"
    left = "left"
    right = "right"


# Naming conventions are ignored to maintain
# code compatibility with the Code Builder.
# noinspection PyPep8Naming
class player:
    @staticmethod
    def position(self):
        pass


def process_api_return(r: requests.Response):
    if r.status_code != 200:
        raise ConnectionError(f"Error connecting to WebAPI:  Status {r.status_code}")
    else:
        j = json.loads(r.content.decode())
        if "errorCode" in j:
            raise RuntimeError(f"Error {j['errorCode']}:  {j['errorMessage']}")
        else:
            return j


def cmd_drop(slot_num: int = 1, quantity: int = 1, direction: str = Directions.forward):
    """
    API Reference:  https://minecraft.fandom.com/wiki/Commands/agent
    """
    params = {"slotNum": slot_num, "quantity": quantity, "direction": direction}
    r = requests.get(f"{BASE_URL}/drop", params=params)
    return process_api_return(r)


def cmd_list():
    """
    API Reference:  https://minecraft.fandom.com/wiki/Commands/list
    """
    raise NotImplementedError("This appears to freeze")
    # r = requests.get(f"{BASE_URL}/list")
    # return process_api_return(r)


def check_connection():
    d = cmd_drop()
    print(d)


if __name__ == "__main__":

    def main():
        check_connection()

    main()
