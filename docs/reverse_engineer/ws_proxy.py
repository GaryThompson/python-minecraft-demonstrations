# Copyright: (c) 2022, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
"""
This script aims to be a proxy between code connect and minecraft itself
"""
import logging
import asyncio

import websockets

from ws_server_trial import (
    is_message_response,
    get_message_request_id,
    is_message_request,
    print_message,
)

logger = logging.getLogger(__name__)

LISTEN_ADDRESS = "localhost"
LISTEN_PORT = 8765
CODE_CONNECT = "ws://localhost:19131"
MSG_WAIT = 1.0

encryption_request = None
encryption_command = None
encryption_response = None
public_key = None


def inspect_encryption(msg: dict):
    """
    This is a tool used to inspect for encryption
    """
    global encryption_request, public_key, encryption_response, encryption_command

    if encryption_request:
        if not encryption_response and is_message_response(msg):
            req_id = get_message_request_id(encryption_request)
            if get_message_request_id(msg) == req_id:
                logger.debug("Encryption response found")
                encryption_response = msg
            else:
                pass
        else:
            # Do nothing, encryption already sorted
            pass
    elif is_message_request(msg):
        body = msg.get("body")
        if body:
            command_line = body.get("commandLine")
            # noinspection SpellCheckingInspection
            if command_line and command_line[:16] == "enableencryption":
                logger.debug("Encryption request found")
                encryption_request = msg
                encryption_command = command_line
    else:
        pass


async def proxy_connection(minecraft_socket: websockets.WebSocketServerProtocol):
    """
    The MineCraft Application has connected to this tool
    """
    logger.debug("New MineCraft connection: %s", minecraft_socket)

    async with websockets.connect(CODE_CONNECT) as code_connect_socket:

        # Relay messages from code connect back to minecraft
        while True:
            # Check for messages from Code Connect to Minecraft
            try:
                while True:
                    msg = await asyncio.wait_for(
                        code_connect_socket.recv(), timeout=MSG_WAIT
                    )
                    print_message("CodeConnect --> Minecraft", msg)
                    await minecraft_socket.send(msg)
            except asyncio.TimeoutError:
                pass

            # Check for messages from Minecraft to Code Connect
            try:
                while True:
                    msg = await asyncio.wait_for(
                        minecraft_socket.recv(), timeout=MSG_WAIT
                    )
                    print_message("CodeConnect <-- Minecraft", msg)
                    await code_connect_socket.send(msg)
            except asyncio.TimeoutError:
                pass


def start_server():
    """
    For the moment, the design is to have the websocket server as the
    main thread and then issue commands in sub-threads.
    """
    logging.getLogger("websockets.server").setLevel(logging.INFO)
    logging.getLogger("websockets.client").setLevel(logging.INFO)
    logging.basicConfig(level=logging.DEBUG)

    async def main():
        async with websockets.serve(proxy_connection, LISTEN_ADDRESS, LISTEN_PORT):
            print(
                "Don't forget to disable encrypted websockets in Settings -> General -> Profile\n"
                "This is available outside of a running game but is locked if a game has started."
            )
            print(f"In Minecraft, type in /connect {LISTEN_ADDRESS}:{LISTEN_PORT}")
            await asyncio.Future()  # run forever

    asyncio.run(main())


if __name__ == "__main__":
    start_server()
