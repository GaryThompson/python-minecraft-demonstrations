========
Research
========

API Options
===========

Minecraft Code Connection
-------------------------

This seems very limited.


Websockets
----------

Links that I have found useful

https://pkg.go.dev/github.com/sandertv/mcwss
https://www.s-anand.net/blog/programming-minecraft-with-websockets/
https://github.com/sanand0/minecraft-websocket/blob/dev/tutorial/mineserver1.py

https://www.reddit.com/r/MCPE/comments/5qf4ah/websockets_huge_potential_currently_unusable/

Asyncio would be ideal for managing this connection, however, as I want to target this library
to a more general audience (especially newer programmers) then not using concurrency can reduce the demand.

But then the standard websocket library is asyncio based, so here we go...


