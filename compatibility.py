# Copyright: (c) 2022, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
"""
This is a compatibility layer to support running
code in two environments.
"""
import time
import logging
import asyncio
from typing import Awaitable
import ws_server_trial

# These are imported for compatibility
# noinspection PyUnresolvedReferences
from ws_server_trial import Position, Block, CloneMask, CloneMode, Axis

number = int
boolean = bool

logger = logging.getLogger(__name__)


def pos(x, y, z) -> Position:
    return Position(x, y, z, is_relative=True)


def world(x, y, z) -> Position:
    return Position(x, y, z, is_relative=False)


def await_async_function(fx: Awaitable):
    """
    The web socket server uses asyncio and through a round
    journey will end up calling functions here that are
    called within the event loop.  However, to maintain
    compatibility with non-async code used as a template,
    this function acts as a bridge between the synchronous
    and asynchronous function styles under the assumption
    that this is all executing within a single thread.
    """
    loop = asyncio.get_running_loop()
    return loop.run_until_complete(fx)


# noinspection PyPep8Naming
class shapes:
    """
    I have not worked out how to access the shapes API
    """


# noinspection PyPep8Naming
class player:
    name: str = None

    @staticmethod
    def say(msg: str):
        print(msg)

    @staticmethod
    def position() -> Position:
        data = await_async_function(
            ws_server_trial.query_target(
                ws_server_trial.compatibility_global_socket, player.name
            )
        )
        pos_data = data[0]["position"]
        return Position(
            x=pos_data["x"] // 1,
            y=pos_data["y"] // 1,
            z=pos_data["z"] // 1,
            is_relative=False,
        )

    @staticmethod
    def on_chat(cmd: str, call_back: callable):
        import ws_server_trial

        ws_server_trial.chat_callbacks[cmd] = call_back

    @staticmethod
    def enable_compatibility():
        """
        Call this method after server connection to enable this
        compatibility class to function
        """
        player.name = await_async_function(
            ws_server_trial.get_local_player_name(
                ws_server_trial.compatibility_global_socket
            )
        )
        logger.info("Set player to: %s", player.name)


# noinspection PyPep8Naming
class positions:
    @staticmethod
    def add(ref: Position, v: Position):
        return ref + v


# noinspection PyPep8Naming
class blocks:
    @staticmethod
    def fill(b_type: Block, start_pos: Position, end_pos: Position):
        await_async_function(
            ws_server_trial.fill(
                ws_server_trial.compatibility_global_socket, start_pos, end_pos, b_type
            )
        )

    @staticmethod
    def clone(
        begin: Position,
        end: Position,
        destination: Position,
        mask: CloneMask,
        mode: CloneMode,
    ):
        await_async_function(
            ws_server_trial.clone(
                ws_server_trial.compatibility_global_socket,
                begin,
                end,
                destination,
                mask,
                mode,
            )
        )


# noinspection PyPep8Naming
class loops:
    @staticmethod
    def pause(ms: int):
        time.sleep(ms / 1000)
