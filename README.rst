===============================
Python Minecraft Demonstrations
===============================

Welcome
=======

Welcome to my Minecraft demonstrations built in Python.  This project is
very much in early development and can be considered as unstable.  It also
may not be maintained.

Why?
====

I wanted to understand if Minecraft can be used for more immersive learning
experiences for students as there are not many programmable virtual world
tools out there (that I have found).  The advantage of Minecraft is that it
is an incredibly popular game and as such its core development is stable.  In
addition, Minecraft is programmable except that I found the built in Blocks,
Typescript (Javascript), and Python to be incredibly limiting.  For legacy
reasons, Minecraft still supports an outbound connection to a websocket
server that is used by code.org and other tools through the Minecraft Code
Connection app.

This project looks at taking advantage of this poorly documented websocket
protocol to develop demonstrations.  At the same time, the main demonstration
scripts are written to be compatible with Minecraft MakeCode in case this
tools becomes difficult to maintain.

Advantages of coding outside of Minecraft include:

* The use of far more sophisticated integrated development environments (IDE)
* The use of the large range of Python libraries (although this will break
  compatibility).
* Debugging outside of the application makes testing a lot easier
* Ability to write unit tests and tests functions
* A richer set of API access functions.
* I find it much easier to write more "pythonic" code - that means easier to
  read (hopefully) - as I have access to more capabilities of the language.
  This should translate to more sophisticated applications with less technical
  debt.


How it works
============

I am using Minecraft Education Edition on Windows 10 with the PyCharms IDE.

With PyCharms, Python, and Poetry it is easy to initialise a new development
environment and simply run the main.py entrypoint.

When you start the program, it will provide connection instructions.  Please note
that sometimes you need to connect from minecraft (calling the /connect command)
a couple of times before it connects.  Once connected, it works fine.

Another note:  You need to disable encrypted websocket connections, TLS has not
yet been implemented in this server.
