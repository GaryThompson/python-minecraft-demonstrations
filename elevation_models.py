"""
Cooler than MakeCode Shapes in Minecraft
"""
from perlin_noise import PerlinNoise


def generate_landscape(width=20, height=1, depth=20):
    """
    See Examples:  https://pypi.org/project/perlin-noise/
    Returns a 2D array where each cell represents the height
    of land at that cell
    """
    noise = PerlinNoise(octaves=2)
    raw_values = [
        [noise([i / width, j / depth]) for j in range(width)] for i in range(depth)
    ]
    min_value = min([min(i) for i in raw_values])
    max_value = max([max(i) for i in raw_values])
    if min_value < 0:
        offset = abs(min_value)
    else:
        offset = 0
    val_range = (max_value - min_value) * 0.7

    norm_values = [
        [
            min(height, int((height + 1) * (raw_values[i][j] + offset) / val_range))
            for j in range(width)
        ]
        for i in range(depth)
    ]
    return norm_values


def blend_landscapes(l1, l2):
    """
    Combines two landscapes to assist with landscape extension
    From trialling, blending is not needed
    """
    # total_width = len(l1[0]) + len(l2[0]) - overlap
    depth = len(l1)
    new_landscape = []
    for row in range(depth):
        segment_1 = l1[row]
        segment_2 = l2[row]
        new_landscape.append(segment_1 + segment_2)

    return new_landscape


if __name__ == "__main__":

    def main():
        import matplotlib.pyplot as plt

        l1 = generate_landscape()
        # l2 = generate_landscape()
        # pic = blend_landscapes(l1, l2)

        plt.imshow(l1, cmap="gray")
        plt.show()

    main()
