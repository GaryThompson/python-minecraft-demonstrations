# Copyright: (c) 2022, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
"""
This file intends to be compatible with the MakeCode version
of Python bundled with Minecraft Education edition.  Note that
the bundled Python is not a true python but is instead
transpiled to TypeScript.
"""
from compatibility import *
from elevation_models import generate_landscape

DEBUG_LEVEL = 10
ERROR_LEVEL = 50
LOG_LEVEL = DEBUG_LEVEL

# As this home-made position class is much easier to use
# then the MakeCode position class, a compatible version will be created
# called "NewPosition" that is API compatible with the home-made version
NewPosition = Position


def debug_log(header, msg):
    if LOG_LEVEL <= DEBUG_LEVEL:
        player.say(
            "DEBUG: %s:  %s"
            % (
                header,
                str(msg),
            )
        )


class WorldParams:
    """
    This is used as a namespace for application settings.  Global variables
    are a bad idea always, but if used a namespace is a better ideas.
    """

    zero_position = world(0, 0, 0)
    # @ts-ignore
    plate1 = None
    # @ts-ignore
    plate2 = None
    # @ts-ignore
    zone = None


def position_string(p: Position):
    return "(%i, %i, %i)" % (
        p.get_value(Axis.X),
        p.get_value(Axis.Y),
        p.get_value(Axis.Z),
    )


def cmd_set_location():
    WorldParams.zero_position = player.position()
    debug_log("Position set to", position_string(WorldParams.zero_position))


def clear_cross_section(ref: Position, height: number, depth: number):
    """
    Clears landscape created with create_cross_section
    """
    end = positions.add(ref, pos(0, height, depth))
    blocks.fill(Block.AIR, ref, end)


class BoundingBox:
    """
    This class represents two corners of a bounding box, often used for moving
    materials on the plate.  Having a dedicated class like this makes
    debug a bit easier.

    The start is always the most negative on all axes: (West, Lower, North)

    Compatibility Note: This is not presently compatible with MakeCode.  As
    pure python is far easier to work with, if this needs to be compatible then
    it might be easier to rewrite the Position class

    Minecraft Nuance:  When providing a start and end bounding box, the end
    is not treated as an upper bound but as the final row in .  This
    has been taken into account
    """

    def __init__(self, p1: NewPosition, p2: NewPosition = None):
        if p2 is None:
            p2 = p1
        # Check which is the west most position
        if not p1.is_relative:
            if p2.is_relative:
                p2 = p1 + p2
        elif not p2.is_relative:
            if p1.is_relative:
                p1 = p2 + p1
        else:
            raise RuntimeError(
                "At least one position in a bounding box must be a world position"
            )

        self.start = NewPosition(
            min(p1.x, p2.x), min(p1.y, p2.y), min(p1.z, p2.z), is_relative=False
        )
        # Ensure the box is always at least 1 in size
        end_x = (p1.x + 1) if p1.x == p2.x else max(p1.x, p2.x)
        end_y = (p1.y + 1) if p1.y == p2.y else max(p1.y, p2.y)
        end_z = (p1.z + 1) if p1.z == p2.z else max(p1.z, p2.z)
        self.end = NewPosition(end_x, end_y, end_z, is_relative=False)

    def __str__(self):
        return (
            f"[{self.start}] to [{self.end}] with "
            f"H: {self.height()}  "
            f"W: {self.width()}  "
            f"D: {self.depth()}."
        )

    def set_box_size(self, width, height, depth):
        self.end = positions.add(self.start, pos(width, height, depth))

    def width(self):
        return abs(self.start.x - self.end.x)

    def height(self):
        return abs(self.start.y - self.end.y)

    def depth(self):
        return abs(self.start.z - self.end.z)

    def slice_y_z(self, x=0):
        """
        Ret returns a new box which is a slice of the current box along the
        Y and Z directions.

        :param x:  A relative x coordinate with the first row being x = 0
        """
        x_pos = self.start.x + x
        new_start = Position(x_pos, self.start.y, self.start.z, is_relative=False)
        new_end = Position(x_pos, self.end.y, self.end.z, is_relative=False)
        return BoundingBox(p1=new_start, p2=new_end)

    def crop_height(self, bottom: number = None, top: number = None):
        """
        Returns a new bounding box that is cropped in the Y direction by
        specifying a relative bottom or top position.
        """
        if bottom:
            new_start = self.start.offset(y_amount=bottom)
        else:
            new_start = self.start

        if top:
            new_end = self.end.offset(y_amount=-1 * top)
        else:
            new_end = self.end
        return BoundingBox(new_start, new_end)

    def crop_width(self, left: number = None, right: number = None):
        """
        Returns a new bounding box that is cropped in the X direction by
        specifying a value relative to both the left and right sides.  In
        other words, if I wanted to shrink the box by 1 on both left and
        right sides the call would be `crop_width(1, 1)`
        """
        if left:
            new_start = self.start.offset(x_amount=left)
        else:
            new_start = self.start

        if right:
            new_end = self.end.offset(x_amount=-1 * right)
        else:
            new_end = self.end
        return BoundingBox(new_start, new_end)

    def taller_box(self, amount):
        """
        Returns a new box with an increased height,  If the amount is negative,
        it lowers the bottom instead of raising the top.
        """
        if amount > 1:
            new_start = self.start
            new_end = self.end.offset(y_amount=amount)
        else:
            new_start = self.start.offset(y_amount=amount)
            new_end = self.end
        return BoundingBox(p1=new_start, p2=new_end)

    def wider_box(self, amount):
        """
        Returns a new box with an increased width.
        A negative number will expand the start side of the box instead of the
        end side.
        """
        if amount > 1:
            new_start = self.start
            new_end = self.end.offset(x_amount=amount)
        else:
            new_start = self.start.offset(x_amount=amount)
            new_end = self.end
        return BoundingBox(p1=new_start, p2=new_end)

    def _minecraft_end(self):
        """
        Note that Minecraft treats the end position as the position that
        includes blocks, not encloses them.  This has been adjusted in this class.
        """
        return self.end - NewPosition(1, 1, 1)

    def fill(self, block_type: Block):
        """
        Fills the enclosed region with the block type.
        """
        blocks.fill(block_type, self.start, self._minecraft_end())

    def move(
        self,
        x_amount=0,
        y_amount=0,
        z_amount=0,
        mask=CloneMask.REPLACE,
        mode=CloneMode.FORCE,
    ):
        """
        Moves all items in the current box leaving Air behind
        """
        new_start = self.start.offset(x_amount, y_amount, z_amount)
        new_end = self.end.offset(x_amount, y_amount, z_amount)
        # Combinations used MASKED, FORCED,
        # Combinations used MASKED, MOVE

        blocks.clone(
            self.start,
            self._minecraft_end(),
            new_start,
            mask,
            mode,
        )
        return BoundingBox(new_start, new_end)


class Plate:
    """
    This class is responsible for tracking a single plate as a whole plate.
    """

    layers = [
        (Block.LAVA, 3),
        (Block.MAGMA_BLOCK, 4),
        (Block.STONE, 3),
        (Block.DIRT, 2),
        (Block.GRASS, 1),
    ]
    random_height = 1

    # @ts-ignore
    def __init__(
        self, start_position: Position, width: number = 10, depth: number = 10
    ):
        self.width = width

        height = Plate.random_height
        for layer in Plate.layers:
            height += layer[1]

        self.bounding_box = BoundingBox(
            start_position, pos(0, 0, 0)  # This is clumsy for compatibility reasons
        )
        self.bounding_box.set_box_size(width, height, depth)
        self._slide_add_x = 0
        self._build_plate()

    def _build_plate(self):
        """
        Constructs the initial plate
        """
        layer_start = 0
        # Build Main layers
        for v in Plate.layers:
            b_type, b_height = v
            layer_end = layer_start + b_height
            layer_box = self.bounding_box.crop_height(
                bottom=layer_start, top=self.bounding_box.height() - layer_end
            )
            layer_box.fill(b_type)
            layer_start = layer_end

        # Decorate the surface
        if Plate.random_height == 0:
            pass  # Do Nothing
        elif Plate.random_height > 1:
            # An efficient way to do this has not been implemented
            raise NotImplementedError
        else:
            # Height is only one
            landscape = generate_landscape(
                self.bounding_box.width(),
                Plate.random_height,
                self.bounding_box.depth(),
            )
            for z, row in enumerate(landscape):
                current_start_position = None
                x_len = 0
                for x, val in enumerate(row):
                    if val == 0 and not current_start_position:
                        pass
                    elif val == 0 and current_start_position:
                        # A series of blocks was recorded,
                        # Place these in one long section
                        random_box = BoundingBox(current_start_position)
                        if x_len > 1:
                            random_box = random_box.wider_box(x_len - 1)
                        random_box.fill(Block.GRASS)

                        current_start_position = None
                        x_len = 0
                    elif not current_start_position:
                        # Val (height of ground) is ignored at the moment
                        # as I haven't found an efficient way to fill a 3D
                        # shape, these for loops are okay for 2D
                        current_start_position = NewPosition(
                            self.bounding_box.start.x + x,
                            self.bounding_box.end.y - 1,  # New start is inclusive
                            self.bounding_box.start.z + z,
                            is_relative=False,
                        )
                    else:
                        x_len += 1

    def move_plate_by(self, x: number, y: number, z: number):
        """
        Moves the plate instantly by the given number of blocks
        """
        self.bounding_box = self.bounding_box.move(x, y, z)

    def slide_surface(self, slide_right: boolean):
        """
        Slides the surface of the plate along the x-axis.  This used to
        remove the end of the section and then slide it across, but this
        resulted in a temporary gap forming in the plate.  What it does now
        is cope the plate across and then adjust the boundaries of the plate
        to ignore the section past the end (leaving the section as is).
        """
        if slide_right:
            self.bounding_box.move(x_amount=1, mode=CloneMode.FORCE)
        else:
            self.bounding_box.move(x_amount=-1, mode=CloneMode.FORCE)

        # Remove the end
        # clear_cross_section(
        #     positions.add(self.pos, pos(remove_x, 0, 0)), self.height, self.depth
        # )
        #

        # # Save values to complete the slide
        # self._slide_add_x = add_x

    def finish_slide_surface(self):
        """
        For performance reasons, when sliding a plate the addition of a new
        layer can be performed independently.
        """
        # Add a start
        # self._create_cross_section(positions.add(self.pos, pos(self._slide_add_x, 0, 0)))


class DestructiveZone:
    """
    This class is responsible for manipulating the destructive zone of two plates
    """

    # @ts-ignore
    def __init__(self, left_plate: Plate, right_plate: Plate):
        self.bounding_box = BoundingBox(
            left_plate.bounding_box.end, right_plate.bounding_box.start
        )
        if self.bounding_box.width() % 2 != 0:
            raise ValueError("The gap between plates needs to be an even number")
        self.left_plate = left_plate
        self.right_plate = right_plate
        self.width_consumed_per_plate = self.bounding_box.width() // 2
        self._build_zone()

        # State variables
        self._captured_sections = 0
        self._total_plates_merged = 0
        self._collision_started = False

    def _build_zone(self):
        """
        Constructs the initial plate
        """
        self.bounding_box.crop_height(
            bottom=0, top=self.bounding_box.height() - 5
        ).fill(Block.LAVA)

    def capture_plates(self):
        """
        Move each plate along into the zone and reduce the boundary
        of the respective plate (so that the plate no longer tracks
        that part of the boundary).

        There are three collision processes as part of this demonstration:

        1. Plates drift into each other and touch
        2. The left plate begins to slide under the right plate
        3. The right plate begins to buckle upwards

        Caution side effect:  Slides both plates towards the centre
        """

        if not self._collision_started and self._captured_sections >= (
            self.bounding_box.width() // 2
        ):
            self._collision_started = True

        # Always move the plates in
        extra_width = self.width_consumed_per_plate - 1
        total_left_side = self.left_plate.bounding_box.wider_box(extra_width)
        total_right_side = self.right_plate.bounding_box.wider_box(-1 * extra_width)
        if not self._collision_started:
            total_left_side.move(
                x_amount=1, mask=CloneMask.REPLACE, mode=CloneMode.FORCE
            )
            total_right_side.move(
                x_amount=-1, mask=CloneMask.REPLACE, mode=CloneMode.FORCE
            )
        else:
            total_left_side.move(
                x_amount=1, mask=CloneMask.MASKED, mode=CloneMode.FORCE
            )
            total_right_side.move(
                x_amount=-1, mask=CloneMask.MASKED, mode=CloneMode.FORCE
            )
            # Plates are now colliding.
            # The left side will move down, starting in the middle and
            # slowly bending each part back the more of the plate that
            # is consumed.
            sheared_sections = min(
                self._total_plates_merged,
                self._total_plates_merged - self.width_consumed_per_plate + 1,
            )
            for i in range(sheared_sections):
                left_shear = self.bounding_box.crop_width(
                    left=self.width_consumed_per_plate - 2 - i,
                    right=self.width_consumed_per_plate
                    + i,  # Change to minus for all sorts of fun
                )
                # This is a little more decorative
                if i == 0:
                    left_shear.move(
                        y_amount=-1, mask=CloneMask.MASKED, mode=CloneMode.MOVE
                    )
                else:
                    left_shear.move(
                        y_amount=-1, mask=CloneMask.MASKED, mode=CloneMode.FORCE
                    )

            # The right side will move the ground up at the surface
            for i in range(sheared_sections):
                combined_width = self.bounding_box.crop_width(
                    left=self.width_consumed_per_plate - i - 1,
                    right=self.width_consumed_per_plate - i - 1,
                )
                land_build = combined_width.crop_height(
                    bottom=self.bounding_box.height() - i - Plate.random_height - 2,
                    top=-1 * self.width_consumed_per_plate,
                )
                land_build.move(y_amount=1, mask=CloneMask.MASKED)

        self._captured_sections = min(
            self._captured_sections + 1, self.width_consumed_per_plate
        )
        self._total_plates_merged += 1


def cmd_init_converge():
    """
    Destructive plates simulation
    """
    cmd_set_location()

    plate_width: number = 75
    plate_depth: number = 40
    destructive_width: number = 10

    debug_log("zero_position at ", str(WorldParams.zero_position))
    base_pos = WorldParams.zero_position.set_y(5).offset(
        x_amount=-1 * (plate_width + destructive_width // 2),
        z_amount=-1 * (plate_depth + 30),
    )

    p1 = Plate(base_pos, plate_width, plate_depth)
    p2 = Plate(
        positions.add(base_pos, pos(plate_width + destructive_width, 0, 0)),
        plate_width,
        plate_depth,
    )
    d_zone = DestructiveZone(p1, p2)

    WorldParams.plate1 = p1
    WorldParams.plate2 = p2
    WorldParams.zone = d_zone


def cmd_loop_converge():
    if WorldParams.plate1 is None:
        cmd_init_converge()

    for _ in range(25):
        # Let the destructive zone capture the plates
        WorldParams.zone.capture_plates()

        # # Grow the plates
        WorldParams.plate1.finish_slide_surface()
        WorldParams.plate2.finish_slide_surface()
        loops.pause(1000)


player.on_chat("init_converge2", cmd_init_converge)
player.on_chat("loop_converge2", cmd_loop_converge)
